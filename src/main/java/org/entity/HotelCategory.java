package org.entity;

import java.io.Serializable;

public class HotelCategory implements Serializable, Cloneable {
	
	private Integer id = 0;
	private String name = "";
	
	public HotelCategory() {
	}
	
	public HotelCategory(Integer id, String name) {
 
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Category: "+name;
	}

	@Override
	protected HotelCategory clone() throws CloneNotSupportedException {
		return (HotelCategory) super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	 
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	 
	public boolean isPersisted() {
		return id != null;
	}
	
	
}

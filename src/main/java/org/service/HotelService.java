package org.service;

import java.util.List;
import java.util.Set;

import org.entity.Hotel;
import org.entity.HotelCategory;

public interface HotelService {
	
	public void addHotel(Hotel hotel);
	
	public void updateHotel(Hotel hotel);
	
	public void addCategory(String category);
	
	public void updateCategory(String update,String source);
	
	public List<String> listCategory();
	
	public List<Hotel> listHotel();
	
	public List<Hotel> find(String name, String address);

	public void removeHotel(Integer id);
	
	public void removeCategory(Set<String> data);
}

package org.service;

import java.util.List;
import java.util.Set;

import org.dao.HotelDAO;
import org.dao.HotelDAOImpl;
import org.entity.Hotel;
import org.entity.HotelCategory;

public class HotelServiceImpl implements HotelService {
 
	private HotelDAO hotelDAO = new HotelDAOImpl();
	
	public void addHotel(Hotel hotel) {
		hotelDAO.add(hotel);
	} 
	
	public List<Hotel> listHotel() {
		return hotelDAO.findAll();
	}

	public void removeHotel(Integer id) {
		hotelDAO.delete(id);
	}

	public void updateHotel(Hotel hotel) {
		hotelDAO.edit(hotel);
	}
 
	public List<Hotel> find(String name, String address) {
		return hotelDAO.find(name, address);
	}

	public void addCategory(String category) {
		hotelDAO.addCategory(category);
	}

	public void updateCategory(String update,String source) {
		hotelDAO.editCategory(update, source);
		
	}

	public List<String> listCategory() {
        return hotelDAO.findAllCategories();
	}

	public void removeCategory(Set<String> data) {
		hotelDAO.deleteCategory(data);
	}

}

package org.dao;

import java.util.List;
import java.util.Set;

import org.entity.Hotel;
import org.entity.HotelCategory;

public interface HotelDAO {
 
    void add(Hotel hotel);
    
    void edit(Hotel holel);
     
    void delete(Integer id);
    
    void addCategory(String hotel);
    
    void editCategory(String update,String source);
     
    void deleteCategory(Set<String> data);
    
    List<String> findAllCategories();
     
    List<Hotel> findAll();
	
    List<Hotel> find(String name, String address);
	
}

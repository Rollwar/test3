package org.task3; 

import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
 
import org.entity.Hotel;
import org.service.HotelServiceImpl;
import org.data.HotelCategoryForm;
import org.data.HotelForm; 

@Theme("mytheme")
public class MyUI extends UI {
 
	private HotelServiceImpl service = new HotelServiceImpl(); 
	private Grid<Hotel> grid = new Grid<>(Hotel.class);	
    private TextField filterName = new TextField();
    private TextField filterAddress = new TextField();
    private HotelForm form = new HotelForm(this,service);
    private HotelCategoryForm category = new HotelCategoryForm(this,service);
    private MenuBar barmenu = new MenuBar();
    private Label selectedMenu = new Label("Hotel Label");

    
    
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
         
        filterName.setPlaceholder("filter by name...");
        filterName.addValueChangeListener(e -> search(filterName,filterAddress));
        filterName.setValueChangeMode(ValueChangeMode.LAZY);

        filterAddress.setPlaceholder("filter by address...");
        filterAddress.addValueChangeListener(e -> search(filterName,filterAddress));
        filterAddress.setValueChangeMode(ValueChangeMode.LAZY);

        Button clearFilterTextBtn = new Button(FontAwesome.TIMES);
        clearFilterTextBtn.setDescription("Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterName.clear());

        Button clearFilterTextBtn2 = new Button(FontAwesome.TIMES);
        clearFilterTextBtn2.setDescription("Clear the current filter");
        clearFilterTextBtn2.addClickListener(e -> filterAddress.clear());
        
        CssLayout filtering = new CssLayout();
        filtering.addComponents(filterName, clearFilterTextBtn);
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        CssLayout filteringAddr = new CssLayout();
        filteringAddr.addComponents(filterAddress, clearFilterTextBtn2);
        filteringAddr.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
        
        Button addHotelBtn = new Button("Add new hotel");
        addHotelBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.switchMode(false);
         form.setHotel(new Hotel(new Integer(5), "", "", new Integer(0),new Long(0), "", "", ""));
        });

        HorizontalLayout toolbar1 = new HorizontalLayout(barmenu,selectedMenu);
        HorizontalLayout toolbar2 = new HorizontalLayout(filtering,filteringAddr,addHotelBtn);
        grid.setColumns("id","name", "category","rating","address","operatesFrom");
        
        grid.addColumn(hotel ->
        "<a href='" + hotel.getUrl() + "' target='_blank'>info</a>",
        new HtmlRenderer()).setCaption("URL");

        
        
        HorizontalLayout main = new HorizontalLayout(grid,form);
        form.setVisible(false);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);

        HorizontalLayout same = new HorizontalLayout(category);
        
        
        layout.addComponents(toolbar1, toolbar2,main,same);
        same.setVisible(false);
        updateTable();

        setContent(layout);

       form.setVisible(false);

        grid.asSingleSelect().addValueChangeListener(event -> {
         
        	if (event.getValue() == null) {
                   form.setVisible(false); 
            } else {
               form.switchMode(true);
               form.setHotel(event.getValue());
            }
     
        });

        MenuBar.Command mycommand = new MenuBar.Command() {
            MenuItem previous = null;

            public void menuSelected(MenuItem selectedItem) {
            	switch(selectedItem.getText())
            	{
            	case "Hotel List":    
            		toolbar2.setVisible(true);
            		main.setVisible(true);
             		same.setVisible(false);
            		selectedMenu.setValue("Hotels");
            		updateTable();
            		break;
            	case "Hotel Categories": 
            		main.setVisible(false);
            		toolbar2.setVisible(false);
             		same.setVisible(true);
            		selectedMenu.setValue("Categories");
                    break;
            	}
        
            }
        };
        
        MenuItem options = barmenu.addItem("Options", null, null);
        options.addItem("Hotel List",      null, mycommand);
        options.addItem("Hotel Categories", null, mycommand);
    
    }
       

    public void updateTable()
    {
    	List<Hotel>  hotels = service.listHotel();
    	grid.setItems(hotels);
        form.refresh();
    }
    
    public void search(TextField tf,TextField tf2)
    {
  //  	List<Hotel>  hotels = service.findByParam(tf.getValue(),tf2.getValue());
 //   	grid.setItems(hotels);
    }
    
    
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}

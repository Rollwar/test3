package org.data;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.vaadin.data.Binder; 
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button; 
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import org.service.Hotel;
import org.service.HotelServiceImpl;
import org.task3.MyUI;

public class HotelCategoryForm extends FormLayout {

	ListSelect<String> select = new ListSelect<>("The List");	
	private Button addCat = new Button("Add"); 
	private Button deleteCat = new Button("Delete");
	private Button editCat = new Button("Edit");
	private Button saveCat = new Button("Save");
	private TextField cat = new TextField();
	private TextField catEdit = new TextField();
	private Button cancel = new Button("Cancel");
	private Button editSubmit = new Button("Set");
	private Button cancelEdit = new Button("Cancel");
	
	private HotelServiceImpl service;
	private Hotel hotel;
	private MyUI myUI;
	
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	
	public HotelCategoryForm(MyUI myUI,HotelServiceImpl service)
	{
		this.myUI = myUI;
		this.service = service;
		VerticalLayout layout = new VerticalLayout();
		
		select.setWidth(350, UNITS_PIXELS);
		cat.setPlaceholder("Enter new category");

        select.setItems(service.listCategory());
		
		select.setDescription("Set hotel's category");
		select.addSelectionListener(event -> {	 
			if(event.getAllSelectedItems().size() > 1)
				editCat.setVisible(false);
			else
				editCat.setVisible(true);
			layout.addComponent(new Label("Selected: " +
	            event.getNewSelection()));
		});
		
		
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(select, addCat, editCat, deleteCat);
		cancel.setStyleName(ValoTheme.BUTTON_QUIET);
		HorizontalLayout tools = new HorizontalLayout(cat,saveCat,cancel);
		HorizontalLayout toolsEdit = new HorizontalLayout(catEdit,editSubmit,cancelEdit);
		tools.setVisible(false);
		toolsEdit.setVisible(false);
		addComponents(select, buttons,tools,toolsEdit);
		
		addCat.setStyleName(ValoTheme.BUTTON_PRIMARY);
		addCat.setClickShortcut(KeyCode.ENTER);
 
		addCat.addClickListener(e -> tools.setVisible(true));
	 	deleteCat.addClickListener(e -> delete()); 
		editCat.addClickListener(e -> {toolsEdit.setVisible(true); 		});
		saveCat.addClickListener(e -> {tools.setVisible(false); add(); cat.clear();  });
		editSubmit.addClickListener(e -> {toolsEdit.setVisible(false); edit(); catEdit.clear();  });
		cancel.addClickListener(e -> {tools.setVisible(false); cat.clear(); });
		cancelEdit.addClickListener(e -> {toolsEdit.setVisible(false); toolsEdit.setVisible(false); cat.clear(); });
	}
 
	
 

	private void delete()
	{
     	service.removeCategory(select.getSelectedItems());
		refresh();
		
	} 
	
	private void add()
	{
		service.addCategory(cat.getValue());
		refresh();
	}
	
	private void edit()
	{
		String set = select.getSelectedItems().iterator().next();
	 	service.updateCategory(catEdit.getValue(),set);
		refresh();
	}
	
	private void refresh()
	{
		select.setItems(service.listCategory());
	}
	
}